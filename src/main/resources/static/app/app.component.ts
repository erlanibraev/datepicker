import { Component, Input, Output } from '@angular/core';

@Component({
    selector: 'my-app',

    template: `
<h1>Test Bootstrap</h1>
{{dateBegin | date : 'dd.MM.yyyy'}}  

- 

{{dateEnd | date : 'dd.MM.yyyy'}}

<datepicker-input [selDate]="dateBegin" (returnDate)="onSelDateBegin($event)" >1</datepicker-input>
<datepicker-input [selDate]="dateEnd" (returnDate)="onSelDateEnd($event)" >1</datepicker-input>


`
})
export class AppComponent {

  public dateBegin: Date = new Date('01.01.1900');
  public dateEnd:Date = new Date('01.01.2999');

  onSelDateBegin(date: Date) {
    this.dateBegin = date;
  }

  onSelDateEnd(date: Date) {
    this.dateEnd = date;
  }

}
