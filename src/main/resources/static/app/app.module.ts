import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import {FormsModule} from "@angular/forms";
import {DatepickerInputComponent} from "./datepickerinput/datepicker-input.component";
import {DatePickerComponent} from "./datepickerinput/datepicker";

@NgModule({
  imports: [ BrowserModule, FormsModule ],
  declarations: [ AppComponent, DatepickerInputComponent, DatePickerComponent ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
